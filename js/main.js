var dssv = [];
var dataJson = localStorage.getItem("DSSV_LOCAL");
if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var sv = new SV(
      item.ma,
      item.ten,
      item.email,
      item.pass,
      item.toan,
      item.ly,
      item.hoa
    );
    dssv.push(sv);
    renderDSSV(dssv);
  }
}

function themSinhVien() {
  var sv = getInfo();
  dssv.push(sv);
  var dataJson = JSON.stringify(dssv);
  localStorage.setItem("DSSV_LOCAL", dataJson);
  resetForm();
  renderDSSV(dssv);
}

function xoaSV(id) {
  var viTri = -1;
  for (var i = 0; i < dssv.length; i++) {
    var sv = dssv[i];
    if (sv.ma == id) {
      viTri = i;
    }
  }
  dssv.splice(viTri, 1);
  var dataJson = JSON.stringify(dssv);
  localStorage.setItem("DSSV_LOCAL", dataJson);
  renderDSSV(dssv);
}

function suaSV(id) {
  var viTri = dssv.findIndex(function (item) {
    return item.ma == id;
  });
  if (viTri != -1) {
    showInfo(dssv[viTri]);
    document.querySelector("#txtMaSV").disabled = true;
  }
}

function capNhatSinhVien() {
  document.querySelector("#txtMaSV").disabled = false;
  var sv = getInfo();
  var viTri = dssv.findIndex((data) => {
    return data.ma == sv.ma;
  });
  if (viTri != -1) {
    dssv[viTri] = sv;
    var dataJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV_LOCAL", dataJson);
    renderDSSV(dssv);
    document.getElementById("formQLSV").reset();
  }
}

function resetForm() {
  document.getElementById("formQLSV").reset();
}

function timSV() {
  var tuKhoa = document.getElementById("txtSearch").value.trim();
  document.getElementById("txtSearch").value = "";

  if(tuKhoa===""){
    renderDSSV(dssv)
    return
  }
  
  var kq = dssv.filter(function(e){
    return e.ten === tuKhoa
  })
  if(kq!=""){
    renderDSSV(kq);
    document.getElementById("spanSearch").innerHTML = ""
  } else{
    document.getElementById("spanSearch").innerHTML = "Không tìm thấy sinh viên"
    renderDSSV(dssv)
  }
};