function getInfo() {
  var ma = document.querySelector("#txtMaSV").value;
  var ten = document.querySelector("#txtTenSV").value;
  var email = document.querySelector("#txtEmail").value;
  var pass = document.querySelector("#txtPass").value;
  var toan = document.querySelector("#txtDiemToan").value * 1;
  var ly = document.querySelector("#txtDiemLy").value * 1;
  var hoa = document.querySelector("#txtDiemHoa").value * 1;

  return new SV(ma, ten, email, pass, toan, ly, hoa);
}

function showInfo(id) {
  document.querySelector("#txtMaSV").value = id.ma;
  document.querySelector("#txtTenSV").value = id.ten;
  document.querySelector("#txtEmail").value = id.email;
  document.querySelector("#txtPass").value = id.pass;
  document.querySelector("#txtDiemToan").value = id.toan;
  document.querySelector("#txtDiemLy").value = id.ly;
  document.querySelector("#txtDiemHoa").value = id.hoa;
}

function renderDSSV(svArr) {
  var table = document.querySelector("#tbodySinhVien");
  var contentHTML = "";
  for (var i = 0; i < svArr.length; i++) {
    var sv = svArr[i];
    var contentTr = `<tr>
                        <td>${sv.ma}</td>
                        <td>${sv.ten}</td>
                        <td>${sv.email}</td>
                        <td>${sv.tinhDTB().toFixed(2)}</td>
                        <td class="d-flex">
                            <button onclick="suaSV(${
                              sv.ma
                            })" class="btn btn-dark"><i class="fa fa-edit"></i></button>
                            <button onclick="xoaSV(${
                              sv.ma
                            })" class="btn btn-danger ml-2"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>`;
    contentHTML += contentTr;
  }
  table.innerHTML = contentHTML;
}
